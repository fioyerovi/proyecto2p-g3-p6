/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loteriamexicana;

import Ventanas.VPrincipal;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class LoteriaMexicana extends Application {
    
    @Override   
    public void start(Stage primaryStage) {
        VPrincipal panel=new VPrincipal();
        Scene scene = new Scene(panel.getRoot(), 500, 420);
        primaryStage.setTitle("Loteria Mexicana");
        primaryStage.setScene(scene);
        primaryStage.show(); 
    }
 
    public static void main(String[] args) {
        launch(args);
    }
}