/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loteriamexicana;

import javafx.scene.image.Image;

/**
 *
 * @author Estudiante
 */
public class Carta {
    
    private int numero;
    private String nombre;
    private Image img; 

    public Carta(int numero, String nombre, Image img) {
        this.numero = numero;
        this.nombre = nombre;
        this.img = img;
    }

    public Image getImg() {
        return img;
    }

    public void setImg(Image img) {
        this.img = img;
    }
    
    
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Carta{" + "numero=" + numero + ", nombre=" + nombre + '}';
    }
   
    
}
