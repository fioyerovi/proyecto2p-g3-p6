/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loteriamexicana;

import java.io.Serializable;

/**
 *
 * @author ReiDina
 */
public class Jugador implements Serializable {
    String fechaDeJuego = "Default" ; 
    String horaDeJuego = "Default" ; 
    String nombre = "Default" ; 
    String oponente = "Default"; 
    String alineacion = "Default";

    public Jugador(String fechaDeJuego, String horaDeJuego, String nombre, String oponente, String alineacion) {
        this.fechaDeJuego = fechaDeJuego;
        this.horaDeJuego = horaDeJuego;
        this.nombre = nombre;
        this.oponente = oponente;
        this.alineacion = alineacion;
    }

    public String getFechaDeJuego() {
        return fechaDeJuego;
    }

    public void setFechaDeJuego(String fechaDeJuego) {
        this.fechaDeJuego = fechaDeJuego;
    }

    public String getHoraDeJuego() {
        return horaDeJuego;
    }

    public void setHoraDeJuego(String horaDeJuego) {
        this.horaDeJuego = horaDeJuego;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getOponente() {
        return oponente;
    }

    public void setOponente(String oponente) {
        this.oponente = oponente;
    }

    public String getAlineacion() {
        return alineacion;
    }

    public void setAlineacion(String alineacion) {
        this.alineacion = alineacion;
    }

    @Override
    public String toString() {
        return "Jugador{" + "fechaDeJuego=" + fechaDeJuego + ", horaDeJuego=" + horaDeJuego + ", nombre=" + nombre + ", oponente=" + oponente + ", alineacion=" + alineacion + '}';
    }
    
    
    
        
}
