package Ventanas;

import java.io.File;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import loteriamexicana.Jugador;

public class VPrincipal{
    private BorderPane root;
    private Jugador jugN ;
    public VPrincipal() {
        this.root = root;
        root=new BorderPane();
        Image img = new Image(new File("src/Img/VentanaInicio.jpg").toURI().toString());
        BackgroundImage imagenFondo = new BackgroundImage(img, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background fondo = new Background(imagenFondo);
        
        root.setBackground(fondo);
        root.setPadding(new Insets(10));
        
        createContent();
        
    }
    public void createContent(){
        
        VBox cntPrincipal = new VBox(15);
        cntPrincipal.setAlignment(Pos.CENTER);
        Label lblTitulo = new Label("Bienvenido a Loteria Mexicana");
         
        Button btnJuegoNuevo = new Button("Juego Nuevo");
        Button btnConfiguracion = new Button("Configuraciones");
        Button btnReporte= new Button("Reporte"); 
        Button btnCerrar = new Button("Cerrar");

        btnJuegoNuevo.setOnAction(e -> {
            abrirVentanaJuego();
        });
        
        btnConfiguracion.setOnAction(e -> {
            abrirVentanaConfiguracion();
        });
        
        btnReporte.setOnAction(e -> {
            abrirVentanaReporte();
        });
        
        btnCerrar.setOnAction(e -> {
            cerrar();
        });
        
        cntPrincipal.getChildren().addAll(lblTitulo, btnJuegoNuevo,btnConfiguracion, btnReporte, btnCerrar);
        root.setCenter(cntPrincipal);    
    }

    public BorderPane getRoot() {
        return root;
    }

    public void setRoot(BorderPane root) {
        this.root = root;
    }
    
    public void abrirVentanaJuego(){
        Stage ventanaNueva = new Stage();
        VJuego ventana = new VJuego();
        Scene scene = new Scene(ventana.getRoot(), 500, 420);
        ventanaNueva.setTitle("Juego Nuevo");
        ventanaNueva.setScene(scene);
        ventanaNueva.show();
    }
    
    public void abrirVentanaConfiguracion(){
        Stage ventanaNueva = new Stage();
        VConfiguracion ventana = new VConfiguracion();
        Scene scene = new Scene(ventana.getRoot(), 500, 420);
        ventanaNueva.setTitle("Configuraciones");
        ventanaNueva.setScene(scene);
        ventanaNueva.show();
    }
    
    public void abrirVentanaReporte(){
        Stage ventanaNueva = new Stage();
        VReporte ventana = new VReporte();
        Scene scene = new Scene(ventana.getRoot(), 500, 420);
        ventanaNueva.setTitle("Reporte");
        ventanaNueva.setScene(scene);
        ventanaNueva.show();
    }
    
    public void cerrar(){
        System.exit(0);
    }
}