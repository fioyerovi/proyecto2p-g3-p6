
package Ventanas;

import Enum.FormaGanar;
import java.io.File;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import loteriamexicana.Jugador;

public class VConfiguracion {
    private BorderPane root;
    ComboBox numeroOponente;
    ComboBox verCartaOponente;
    public VConfiguracion() {
        this.root = root;
        root=new BorderPane();
        
        Image img = new Image(new File("src/Img/VentanaInicio.jpg").toURI().toString());
        BackgroundImage imagenFondo = new BackgroundImage(img, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background fondo = new Background(imagenFondo);
        
        root.setBackground(fondo);
        
        
        
        root.setPadding(new Insets(130));
        createContent();
    }
    public void createContent(){
        
        VBox cntPrincipal = new VBox(15);
         
        HBox cntTitulo = new HBox(5);
        Label lblTitulo = new Label("CONFIGURACIONES");
        cntTitulo.getChildren().add(lblTitulo);
        cntTitulo.setAlignment(Pos.CENTER);
        
        HBox cntCantOponente = new HBox(5);
        Label lblCantOponente = new Label("Cantidad de Oponentes");
        lblCantOponente.setPadding(new Insets(0, 0, 0, 15));
        numeroOponente = new ComboBox();
        numeroOponente.getItems().setAll("1","2");
        cntCantOponente.getChildren().addAll(lblCantOponente, numeroOponente);
        cntCantOponente.setAlignment(Pos.CENTER);
        
        HBox cntVerCartaOponente = new HBox(5);
        Label lblVerCartaOponente = new Label("Visibilidad");
        lblVerCartaOponente.setPadding(new Insets(0, 0, 0, 15));
        verCartaOponente = new ComboBox();
        verCartaOponente.getItems().setAll("VISIBLE","OCULTO");
        cntVerCartaOponente.getChildren().addAll(lblVerCartaOponente, verCartaOponente);
        cntVerCartaOponente.setAlignment(Pos.CENTER);
        
        HBox cntBotones = new HBox(5);  
        Button btnGuardar = new Button("Guardar");
        btnGuardar.setOnAction((ActionEvent e) -> {
            System.out.println("Guardar");
        });
        Button btnCerrar = new Button("Cerrar");
        btnCerrar.setOnAction(e -> {
            System.exit(0);
        }); 
        cntBotones.getChildren().addAll(btnGuardar,btnCerrar);
        cntBotones.setAlignment(Pos.CENTER);
        
        cntPrincipal.getChildren().addAll(cntTitulo,cntCantOponente,cntVerCartaOponente,cntBotones);
        root.setCenter(cntPrincipal);    
    }

    public BorderPane getRoot() {
        return root;
    }

    public void setRoot(BorderPane root) {
        this.root = root;
    }
    
}
