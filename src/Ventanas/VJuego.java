
package Ventanas;

import Enum.FormaGanar;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import loteriamexicana.Jugador;

public class VJuego {
    private BorderPane root;
    private Jugador jugN;
    TextField txtNombre;
    ComboBox cmbFormaGanar;
    VReporte vreporte;
    public ArrayList<Jugador> jugadores;
    String filename = "jugadores.ser";
    
    public VJuego() {
        cargarJugador();
        System.out.println(jugadores);
        root=new BorderPane();
        root.setPadding(new Insets(130));
        
        Image img = new Image(new File("src/Img/VentanaInicio.jpg").toURI().toString());
        BackgroundImage imagenFondo = new BackgroundImage(img, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background fondo = new Background(imagenFondo);
        
        root.setBackground(fondo);
        createContent();
    }
    
    public void createContent(){        
        VBox cntPrincipal = new VBox(15);
        
        HBox cntTitulo = new HBox(5);
        Label lblTitulo = new Label("JUEGO NUEVO");
        cntTitulo.getChildren().add(lblTitulo);
        cntTitulo.setAlignment(Pos.CENTER);
        
        HBox cntNuevoJugador = new HBox(5);
        Label lblNombreJugador = new Label("Nombre");
        lblNombreJugador.setPadding(new Insets(0, 0, 0, 15));
        txtNombre = new TextField();
        txtNombre.setPadding(new Insets(0, 0, 0, 15));
        cntNuevoJugador.getChildren().addAll(lblNombreJugador, txtNombre);
        cntNuevoJugador.setAlignment(Pos.CENTER);
        
                
        HBox cntBotones = new HBox(5);
        Button btnComenzar = new Button("Comenzar");
        btnComenzar.setOnAction(e -> {    
            abrirVentanaJuegoMain();
        });
        Button btnCerrar = new Button("Cerrar");
        btnCerrar.setOnAction(e -> {
            Cerrar();
        });
        cntBotones.getChildren().addAll(btnComenzar, btnCerrar);
        cntBotones.setAlignment(Pos.CENTER);
        
        cntPrincipal.getChildren().addAll(cntTitulo,cntNuevoJugador,cntBotones);
        
        root.setCenter(cntPrincipal);    
    }

    public BorderPane getRoot() {
        return root;
    }

    public void setRoot(BorderPane root) {
        this.root = root;
    }
    
    public void abrirVentanaJuegoMain(){
        Calendar fecha = new GregorianCalendar();
        int año = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);
        int segundo = fecha.get(Calendar.SECOND);
        String fechaDeJuego = ""+ dia + "/" + (mes+1) + "/" + año;
        String horaDeJuego = ""+ hora + "/" + minuto + "/" + segundo;
        String nombre = txtNombre.getText();
        
        if (nombre.length() > 0 ){
            Jugador jugador = new Jugador(fechaDeJuego, horaDeJuego, nombre, "1", nombre );
            jugadores.add(jugador);
            actualizarArchivoJugadores();     
            
            
            //VJuegoMain ventana = new VJuegoMain();
            JuegoMain ventana = new JuegoMain();
            
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("UPS!");
            alert.setContentText("Todos los campos son obligatorios :\"V");
            alert.showAndWait();
        }   
    }
    
     public void actualizarArchivoJugadores(){
        FileOutputStream fout = null; 
        try {
            fout = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(fout);
            out.writeObject(jugadores);
            out.flush();
            fout.close();
        }catch(FileNotFoundException ex){
            System.out.println(ex.getMessage());
        }catch(IOException ex){
            System.out.println(ex.getMessage());
            System.out.println("No se serializo");
        }finally{
            try{
                fout.close();
            }catch(IOException ex){
                System.out.println(ex.getMessage());
            }
        }
    }
     
        private void cargarJugador(){
        jugadores = new ArrayList<>();
        Path path = Paths.get(filename);
        if(Files.exists(path)){
            ObjectInputStream in = null;
            try{
                in = new ObjectInputStream(new FileInputStream(filename));
                jugadores = (ArrayList<Jugador>) in.readObject();
                in.close();
            } catch (FileNotFoundException ex){
                System.out.println(ex.getMessage());
            } catch (IOException ex){
                System.out.println(ex.getMessage());
            } catch (ClassNotFoundException ex){
                System.out.println(ex.getMessage());
            } finally{
                try{
                    in.close();
                } catch (IOException ex){
                    System.out.println(ex.getMessage());
                }
            }
        }
        System.out.println(jugadores);
    }
    
    public void Cerrar(){
        System.exit(0);
    }
}
