
package Ventanas;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import loteriamexicana.Jugador;

public class VReporte {
    
    BorderPane root;
    TableView tableView;
    public ArrayList<Jugador> jugadores;
    String filename = "jugadores.ser";
    
    public VReporte(){
        cargarJugador();
        createContent();
    }
    
    private void createContent(){      
        root = new BorderPane();
        String[] columnas = {"Fecha","Hora","Jugador","#Oponentes","Alineacion"};
        tableView =new TableView();
        for(String campo:columnas){
            TableColumn<String,Jugador> column = new TableColumn<>(campo);
            column.setCellValueFactory(new PropertyValueFactory<>(campo.toLowerCase()));
            tableView.getColumns().add(column);   
        }for(Jugador j:jugadores){
            tableView.getItems().add(j);
        }
        
        HBox cntTitulo = new HBox(10);
        Label lblTitulo = new Label("TABLA DE PUNTUACIONES");
        cntTitulo.getChildren().add(lblTitulo);
        cntTitulo.setAlignment(Pos.CENTER);
        
        //Contenedor botones
        HBox cntBotones = new HBox(10);
        Button btnCerrar = new Button("Cerrar");
        btnCerrar.setOnAction(e->{
           btnCerrar.getScene().getWindow().hide();
        });
        cntBotones.getChildren().add(btnCerrar);
        cntBotones.setAlignment(Pos.CENTER);
        
        root.setTop(cntTitulo);
        root.setCenter(tableView);
        root.setBottom(cntBotones);
    }
    
    public BorderPane getRoot(){
        return root;
    }
    
    private void cargarJugador(){
        jugadores = new ArrayList<>();
        Path path = Paths.get(filename);
        if(Files.exists(path)){
            ObjectInputStream in = null;
            try{
                in = new ObjectInputStream(new FileInputStream(filename));
                jugadores = (ArrayList<Jugador>) in.readObject();
                in.close();
            } catch (FileNotFoundException ex){
                System.out.println(ex.getMessage());
            } catch (IOException ex){
                System.out.println(ex.getMessage());
            } catch (ClassNotFoundException ex){
                System.out.println(ex.getMessage());
            } finally{
                try{
                    in.close();
                } catch (IOException ex){
                    System.out.println(ex.getMessage());
                }
            }
        }
        System.out.println(jugadores);
    }
    
    public void actualizarTableView(){
        tableView.getItems().clear();
        //agregar los datos del arraylist al tableview
        for(Jugador j: jugadores){
            tableView.getItems().add(j);
        }
    }
    
   
}
