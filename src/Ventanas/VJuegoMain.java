
package Ventanas;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class VJuegoMain {
    private BorderPane root;
    ComboBox numeroOponente;
    ComboBox verCartaOponente;
    
    public VJuegoMain() {
        this.root = root;
        root=new BorderPane();
        root.setPadding(new Insets(10));
        createContent();
    }
    public void createContent(){
        
        HBox cntPrincipal = new HBox(3);
        cntPrincipal.setAlignment(Pos.CENTER);
         
        //Contenedor Izquierda
        VBox cntIzquierdo = new VBox(15);
        Label lblFormaGanar = new Label("Aqui va la imagen de la forma de ganar");
        GridPane tablaOponente = new GridPane();
        tablaOponente.setPadding(new Insets(20));
        tablaOponente.setHgap(25);
        tablaOponente.setVgap(15);
        for(int j=0;j<4;j++){
            for(int i=0;i<4;i++){
                Label lblPos = new Label("("+i+","+j+")");
                tablaOponente.add(lblPos, i, j);
            }
        }
        cntIzquierdo.getChildren().addAll(lblFormaGanar, tablaOponente);
        cntIzquierdo.setAlignment(Pos.CENTER);
        
        //Contenedor Centro
        VBox cntCentral = new VBox(15);
        Label lblTablaJugador = new Label("Cartilla del Jugador");
        GridPane tablaJugador = new GridPane();
        tablaJugador.setPadding(new Insets(20));
        tablaJugador.setHgap(25);
        tablaJugador.setVgap(15);
        for(int j=0;j<4;j++){
            for(int i=0;i<4;i++){
                VBox cntCasillaCarta = new VBox(5);
                Label lblImagen = new Label("Imagen");
                Label lblPos = new Label("("+i+","+j+")");
                Button btnMarcar = new Button("Marcar");
                cntCasillaCarta.getChildren().addAll(lblImagen,lblPos,btnMarcar);
                tablaJugador.add(lblPos, i, j);
            }
        }
        cntCentral.getChildren().addAll(lblTablaJugador,tablaJugador);
        cntCentral.setAlignment(Pos.CENTER);
        
        //Contenedor Derecho
        VBox cntDerecho = new VBox(15);
        Label lblCarta = new Label("Carta");
        Label lblCarta2 = new Label("imagen de la carta");
        lblCarta.setPadding(new Insets(0, 0, 0, 15));
        Button btnGanar = new Button("Ganar");
        btnGanar.setOnAction((ActionEvent e) -> {
            System.out.println("Guardar");
        });
        
        cntDerecho.getChildren().addAll(lblCarta,lblCarta2, btnGanar);
        cntDerecho.setAlignment(Pos.CENTER);
        
        //Añade todo al contenedor principal y luego a la raiz
        cntPrincipal.getChildren().addAll(cntIzquierdo,cntCentral,cntDerecho);
        root.setCenter(cntPrincipal);    
    }

    public BorderPane getRoot() {
        return root;
    }

    public void setRoot(BorderPane root) {
        this.root = root;
    }
    
}
