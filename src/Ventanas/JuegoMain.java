/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import Enum.FormaGanar;
import static Enum.FormaGanar.COLUMN4;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.PauseTransition;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import loteriamexicana.Carta;
import loteriamexicana.Jugador;
import loteriamexicana.LoteriaMexicana;

/**
 *
 * @author ReiDina
 */
public class JuegoMain {
    private BorderPane root ;
    private Jugador jugDef ;
    Label lblAux = new Label();
    BorderPane central1 = new BorderPane();
    GridPane central = new GridPane();
    GridPane gridPc = new GridPane(); 
    String frmGanarTot = new String();
    int[][] identificadorImg = new int[4][4];
    int [][] frmGanar = new  int[4][4];
    int [][] frmGanar1 = new  int[4][4];
    int[][] identificadorImgPC = new int[4][4];
    ArrayList<Carta> cartasPc ;
    int numero[] = new int[54]; 
    StackPane auxStack[][] = new StackPane[4][4];
    StackPane auxStackPC[][] = new StackPane[4][4];
    
    FormaGanar frmWin = dameFormaGanar();
    String formasG= frmWin.toString();
    
    public JuegoMain() {
        this.root = root;
        root = new BorderPane();
        
        root.setPadding(new Insets(10));
        fillcenter();
    }
    
    public void fillcenter(){
        Stage juego = new Stage();
        Image img = new Image(new File("src/Img/fondoJuego.jpg").toURI().toString());
        BackgroundImage imagenFondo = new BackgroundImage(img, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        Background fondoImg = new Background(imagenFondo);
        
        
        HBox fondo = new HBox(50);
        fondo.setBackground(fondoImg);
        fondo.setPadding(new Insets(20, 12, 15, 12));
        /**/
        ProgressBar pb = new ProgressBar();
        pb.setProgress(1);
        /**/
        
        /***Parte Izquierda***/
        VBox partLeft = new VBox(40);
        
        
        Image[] imgFrmGanar = new Image[4];
        File[] urlImgs = new File[4];
        
        urlImgs[0] = new File("src/Img/FILA4.jpg");
        urlImgs[1] = new File("src/Img/ESQUINAS4.jpg");
        urlImgs[2] = new File("src/Img/COLUMN4.jpg");
        urlImgs[3] = new File("src/Img/JUNTAS4.jpg");
        
        
        try {
            for(int i = 0; i<4;i++){
                imgFrmGanar[i] = new Image(urlImgs[i].toURI().toString());
            }
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
        
        ImageView imgV = new ImageView();
        imgV.setFitHeight(400);
        imgV.setFitWidth(250);
        /*Imagenes */
        switch (formasG){
                    
            case "FILAS4":
                imgV = new ImageView(imgFrmGanar[0]);
                imgV.setFitHeight(400);
                imgV.setFitWidth(250);
                break;
            case "ESQUINAS4":
                imgV = new ImageView(imgFrmGanar[1]);
                imgV.setFitHeight(400);
                imgV.setFitWidth(250);
                break;
            case "COLUMN4":
                imgV = new ImageView(imgFrmGanar[2]);
                imgV.setFitHeight(400);
                imgV.setFitWidth(250);
                break;
            case "JUNTAS4":
                imgV = new ImageView(imgFrmGanar[3]);
                imgV.setFitHeight(400);
                imgV.setFitWidth(250);
                break;
            default:
                System.out.println("Defecto");
                
        }
        
        
       
       
        
        /**Dependiendo de la forma de ganar iremos a escoger una imagen**/
        /*
        Image image = null  ;
        try {
            image = new Image(new FileInputStream("src\\Img\\narutoPrueba.jpg"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(LoteriaMexicana.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        
        /**Este seria el mismo tablero del juego en pero para la pc**/
        
        cartasPc = dameCartas(16);
        Collections.shuffle(cartasPc);
        gridPc.setHgap(10);
        gridPc.setVgap(10);
        int[] aparecidos1 = new int[16];
        int aleatorio1 = 0  ;
        for(int i = 0 ; i<4; i++){
            for(int j = 0 ; j < 4 ; j++){
                
                
                ImageView contImg = new ImageView(cartasPc.get(aleatorio1).getImg());
                ImageView contImg1 = new ImageView(new File("src/Img/frejol.png").toURI().toString());
                
                contImg1.setFitHeight(20);
                contImg1.setFitWidth(20);
                contImg1.setVisible(false);
                contImg.setFitHeight(45);
                contImg.setFitWidth(45);
                auxStackPC[i][j] = new StackPane();
                auxStackPC[i][j].getChildren().addAll(contImg,contImg1);
                gridPc.add(auxStackPC[i][j], i, j);
                identificadorImgPC[i][j] = cartasPc.get(aleatorio1).getNumero();
                //identificadorImg[i][j] = cartasPc.get(aleatorio1).getNumero();
                //Para el caso de la pc solo vamos a necesitar saber si esta dentro de las
                //cartas que estan a posesion de la pc
                aleatorio1 ++;
            }
            
        }
        gridPc.setAlignment(Pos.CENTER);
        partLeft.getChildren().addAll(imgV,gridPc);
        /*********/
        ArrayList<Carta> cartasCentral = new ArrayList<>();
        cartasCentral = dameCartas(0);
        Collections.shuffle(cartasCentral);
        
        int[][] identificadorImg = new int[4][4];
        TextField txt2 = new TextField();
        txt2.setPromptText("Ingresa nombre");
        txt2.setAlignment(Pos.CENTER);
        central.setHgap(10);
        central.setVgap(10);
        int[] aparecidos = new int[16];
        int aleatorio =0 ;
        for(int i = 0 ; i<4; i++){
            for(int j = 0 ; j < 4 ; j++){
                ImageView contImg = new ImageView(cartasCentral.get(aleatorio).getImg());
                ImageView contImg1 = new ImageView(new File("src/Img/frejol.png").toURI().toString());
                contImg1.setFitHeight(30);
                contImg1.setFitWidth(30);
                contImg1.setVisible(false);
                contImg.setFitHeight(130);
                contImg.setFitWidth(90);
                auxStack[i][j] = new StackPane();
                auxStack[i][j].getChildren().addAll(contImg,contImg1);
                central.add(auxStack[i][j], i, j);
                identificadorImg[i][j] = cartasCentral.get(aleatorio).getNumero();
                aleatorio ++ ;
            }
        }
        
        
        System.out.println(identificadorImg);
        
        
        central.setAlignment(Pos.CENTER);
        
        /************************************************************/
        
        /************************************************************/
        BorderPane  frejoles = new BorderPane();
        
        ArrayList<Carta> cartas ;
        cartas = dameCartas(0);
        
        ArrayList<Image> imagenesPrueba = new ArrayList<>();
        
        Collections.shuffle(cartas);
        for (int i = 0; i<54 ; i++){
            imagenesPrueba.add(cartas.get(i).getImg());
            numero[i] = cartas.get(i).getNumero();
        }
        
        ImageView contImg = new ImageView();
        
        final int NUM_FRAMES = 54;
        final int PAUSE_BETWEEN_FRAMES = 3;

        ImageView imageView = new ImageView();
        imageView.setFitHeight(500);
        imageView.setFitWidth(300);
        Timeline timeline = new Timeline();
     //   List<Image> images = (List<Image>)imagenesPrueba;
        
        for (int i = 0; i < NUM_FRAMES; i++) {
            timeline.getKeyFrames().add(
                new KeyFrame(
                            javafx.util.Duration.seconds(i * PAUSE_BETWEEN_FRAMES), 
                    new KeyValue(imageView.imageProperty(), imagenesPrueba.get(i))
                )
            );
        }
        /*Si se pueede agregar el progress bar*/
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
        frejoles.setCenter(imageView);
        ImageView[][] frejojes1 = new ImageView[4][4];
        for(int i = 0 ; i<4; i++){
            for(int j = 0 ; j < 4 ; j++){
                frejojes1[i][j]=new ImageView(new Image(new File("src/Img/frejol.jpg").toURI().toString()));
        }
        }
        
        lblAux.setOnMouseClicked(e->{
            System.out.println(lblAux.getText());
        });
        central.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                Node result = null;
                ObservableList<Node> childrens = central.getChildren();
                int iverifica = 0;
                int noHay =0;
                for(Node apunNode: childrens){
                    if (String.valueOf(identificadorImg[central.getRowIndex(apunNode)][central.getColumnIndex(apunNode)]).equalsIgnoreCase(lblAux.getText())){
                        System.out.println("Carta coincide en ");
                        System.out.println("("+central.getRowIndex(apunNode)+","+central.getColumnIndex(apunNode)+")");
                        System.out.println(identificadorImg[central.getRowIndex(apunNode)][central.getColumnIndex(apunNode)]);
                        frmGanar[central.getRowIndex(apunNode)][central.getColumnIndex(apunNode)] = 1;
                        auxStack[central.getRowIndex(apunNode)][central.getColumnIndex(apunNode)].getChildren().set(1, new ImageView(new Image(new File("src/Img/frejol.png").toURI().toString())));
                        noHay=1;
                    }else{
                        
                    }
                    iverifica++;
                }
                if (noHay==0){
                    ImageView noHayImg= new ImageView(new Image(new File("src/Img/noHay.png").toURI().toString()));
                        
                    PauseTransition delay = new PauseTransition(javafx.util.Duration.seconds(1));
                    delay.setOnFinished(ex-> noHayImg.setVisible(false));
                    frejoles.getChildren().add(noHayImg);
                    
                    delay.play();
                }
            }
        });
        
        central1.setCenter(central);
////        central1.setCenter(frejojes1);
        /*************************************************************/
        BorderPane boton = new BorderPane();
        
        VBox parteDere = new VBox();
        ImageView botonGanar = new ImageView(new Image(new File("src/Img/botonG.png").toURI().toString()));
        parteDere.setAlignment(Pos.CENTER);
        
        parteDere.getChildren().add(botonGanar);
        botonGanar.setOnMouseClicked((e->{           
            if("COLUMN4".equals(formasG) && existen4Columnas()){
                System.out.println("Gano Jugador por hacer una columna");
                System.exit(0);
            }else if ("FILAS4".equals(formasG) && existen4Filas()){
                System.out.println("Gano Jugador por hacer una fila");
                System.exit(0);
            }else if ("JUNTAS4".equals(formasG) && existen4Esquinas()){
                System.out.println("Gano Jugador por hacer una Esquina");
                System.exit(0);
            }else if("ESQUINAS4".equals(formasG) && existen4Puntas()){
                System.out.println("Gano Jugador por 4 una Esquina");
                System.exit(0);
            }else{
                System.out.println("Mira Bien");
            }
        }));
        /*Cambiar estilo del boton*/
        /************************************/
        fondo.getChildren().addAll(partLeft,central,frejoles,parteDere);
        Scene scene = new Scene(fondo, 1200 , 670);
        scene.getStylesheets().add("Css/buton.css");
        juego.setScene(scene);
        juego.setTitle("Juego Nuevo");
        juego.setResizable(false);
        juego.setOnShown(e->{
            
            movimientolblAux t1 = new movimientolblAux();
            Thread mov = new Thread(t1);
            mov.start();
            
            jugadorPc t2 = new jugadorPc();
            Thread mov1 = new Thread(t2);
            mov1.start();
            
            
            System.out.println("Si");
            
            
        });
        
        juego.show();
        
    }
    
    public BorderPane getRoot() {
        return root;
    }

    public void setRoot(BorderPane root) {
        this.root = root;
    }

    private FormaGanar dameFormaGanar() {
        Random random = new Random();
        int indice=random.nextInt(3);
        int cont = 0; 
        FormaGanar fg1 = null ;
        for(FormaGanar fg: FormaGanar.values()){
            if (cont == indice){
                fg1 = fg;
            }
            cont ++;
        }
        System.out.println(fg1);
        return fg1;
    }
    
//    public boolean verificaSiGano(String frmGanar){
//      frmGanar = "4COLUMNAS";
//     switch(frmGanar){
//             case "4COLUMNAS":
//                  boolean a = existen4Columnas();
//                  return  a ;
//             default:
//                 return false;
//                 
//     }
//     
// }
    public boolean existen4Columnas(){
     
     if(frmGanar[0][0]==1 && frmGanar[1][0]==1 && frmGanar[2][0]==1 && frmGanar[3][0]==1){
         return true ;
     }else if (frmGanar[0][1]==1 && frmGanar[1][1]==1 && frmGanar[2][1]==1 && frmGanar[3][1]==1){
         return true ;
     }else if(frmGanar[0][2]==1 && frmGanar[1][2]==1 && frmGanar[2][2]==1 && frmGanar[3][2]==1){
         return true ;
     }else if(frmGanar[0][3]==1 && frmGanar[1][3]==1 && frmGanar[2][3]==1 && frmGanar[3][3]==1){
         return true ;
     }else{
         return false ;
     }
 }
    public boolean existen4ColumnasPc(){
     
     if(frmGanar1[0][0]==1 && frmGanar1[1][0]==1 && frmGanar1[2][0]==1 && frmGanar1[3][0]==1){
         return true ;
     }else if (frmGanar1[0][1]==1 && frmGanar1[1][1]==1 && frmGanar1[2][1]==1 && frmGanar1[3][1]==1){
         return true ;
     }else if(frmGanar1[0][2]==1 && frmGanar1[1][2]==1 && frmGanar1[2][2]==1 && frmGanar1[3][2]==1){
         return true ;
     }else if(frmGanar1[0][3]==1 && frmGanar1[1][3]==1 && frmGanar1[2][3]==1 && frmGanar1[3][3]==1){
         return true ;
     }else{
         return false ;
     }
     
     
     
     
 }
    public boolean existen4Filas(){
     
     if(frmGanar[0][0]==1 && frmGanar[0][1]==1 && frmGanar[0][2]==1 && frmGanar[0][3]==1){
         return true ;
     }else if (frmGanar[1][0]==1 && frmGanar[1][1]==1 && frmGanar[1][2]==1 && frmGanar[1][3]==1){
         return true ;
     }else if(frmGanar[2][0]==1 && frmGanar[2][1]==1 && frmGanar[2][2]==1 && frmGanar[2][3]==1){
         return true ;
     }else if(frmGanar[3][0]==1 && frmGanar[3][1]==1 && frmGanar[3][2]==1 && frmGanar[3][3]==1){
         return true ;
     }else{
         return false ;
     }
    }
    public boolean existen4FilasPc(){
     
    if(frmGanar1[0][0]==1 && frmGanar1[0][1]==1 && frmGanar1[0][2]==1 && frmGanar1[0][3]==1){
         return true ;
     }else if (frmGanar1[1][0]==1 && frmGanar1[1][1]==1 && frmGanar1[1][2]==1 && frmGanar1[1][3]==1){
         return true ;
     }else if(frmGanar1[2][0]==1 && frmGanar1[2][1]==1 && frmGanar1[2][2]==1 && frmGanar1[2][3]==1){
         return true ;
     }else if(frmGanar1[3][0]==1 && frmGanar1[3][1]==1 && frmGanar1[3][2]==1 && frmGanar1[3][3]==1){
         return true ;
     }else{
         return false ;
     }
     
     
     
     
 }
    public boolean existen4Esquinas(){
      if(frmGanar[0][0]==1 && frmGanar[0][1]==1 && frmGanar[1][0]==1 && frmGanar[1][1]==1){
         return true ;
     }else if (frmGanar[0][2]==1 && frmGanar[0][3]==1 && frmGanar[1][2]==1 && frmGanar[1][3]==1){
         return true ;
     }else if(frmGanar[2][0]==1 && frmGanar[2][1]==1 && frmGanar[3][0]==1 && frmGanar[3][1]==1){
         return true ;
     }else if(frmGanar[2][2]==1 && frmGanar[2][3]==1 && frmGanar[3][2]==1 && frmGanar[3][3]==1){
         return true ;
     }else{
         return false ;
     }
    }
    public boolean existen4EsquinasPc(){
     if(frmGanar1[0][0]==1 && frmGanar1[0][1]==1 && frmGanar1[1][0]==1 && frmGanar1[1][1]==1){
         return true ;
     }else if (frmGanar1[0][2]==1 && frmGanar1[0][3]==1 && frmGanar1[1][2]==1 && frmGanar1[1][3]==1){
         return true ;
     }else if(frmGanar1[2][0]==1 && frmGanar1[2][1]==1 && frmGanar1[3][0]==1 && frmGanar1[3][1]==1){
         return true ;
     }else if(frmGanar1[2][2]==1 && frmGanar1[2][3]==1 && frmGanar1[3][2]==1 && frmGanar1[3][3]==1){
         return true ;
     }else{
         return false ;
     }
    }
     public boolean existen4Puntas(){
     if(frmGanar[0][0]==1 && frmGanar[0][3]==1 && frmGanar[3][0]==1 && frmGanar[3][3]==1){
         return true ;
     }else{
         return false ;
     }
    }
      public boolean existen4PuntasPc(){
     if(frmGanar1[0][0]==1 && frmGanar1[0][3]==1 && frmGanar1[3][0]==1 && frmGanar1[3][3]==1){
         return true ;
     }else{
         return false ;
     }
    }
    
    
    private ArrayList<Carta> dameCartas(int j) {
        /*Hacer un shuffle en las imagenes*/
       Image[] imgFrmGanar = new Image[8];
       File[] urlImgs = new File[54];
       ArrayList<Carta> cartas = new ArrayList<>();
       for(int i =0 ; i<54 ; i++){
           urlImgs[i] = new File("src/Img/"+(i+1)+".jpg");
           cartas.add(new Carta((i+1), "Carta1", new Image(urlImgs[i].toURI().toString())));
           
       }
       if (j>0){
           ArrayList<Carta> cartas1 = new ArrayList<>();
           for(int i =0 ; i<54 ; i++){
           urlImgs[i] = new File("src/Img/"+(i+1)+".jpg");
           cartas.add(new Carta((i+1), "Carta1", new Image(urlImgs[i].toURI().toString())));
           
            }
           Collections.shuffle(cartas);
           for(int i =0 ; i<j; i++){
           cartas1.add(cartas.get(i));
            }
           return cartas1;
       }
       
       
       
       Collections.shuffle(cartas);
       return cartas;
       
    }

    private FadeTransition createFader(ImageView noHay) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
 public class movimientolblAux implements Runnable{
    
    boolean detener = false ; 
    int i = 0; 
    @Override
     
    public void run() {
           
            while(!detener){
                
                    Platform.runLater(new Runnable() {
                    @Override
                   
                        public void run() {
                         
                        if ( i < 54){     
                        lblAux.setText(String.valueOf(numero[i]));
                        System.out.println(numero[i]);
                        }else{
                            detener = true ;
                        }
                        }
                    
                });try {
                    Thread.sleep(3000);
                } catch (InterruptedException ex) {
                    System.out.println(ex.getMessage());
                }
                i++;
                }
                
                System.out.println(i);
            }
    public void detener(){
        detener = true ;    
    }
        }
 public class jugadorPc implements Runnable{

        boolean detener = false;
        int i = 0 ;  
        public void run() {
          
            while(i<54 && !detener){
                
                    Platform.runLater(new Runnable() {
                    @Override
                   
                        public void run() { 
                            for(Carta apunCarta: cartasPc){
                                if (String.valueOf(apunCarta.getNumero()).equalsIgnoreCase(lblAux.getText())){
                                    System.out.println("Pc tiene una carta apresurate");
                                    System.out.println("Carta pc: "+apunCarta.getNumero());
                                    System.out.println("Carta en el juego " + lblAux.getText());
                                    ObservableList<Node> gridPch = gridPc.getChildren();
                                    for(Node apunNode: gridPch){
                                      if (String.valueOf(identificadorImgPC[gridPc.getRowIndex(apunNode)][gridPc.getColumnIndex(apunNode)]).equalsIgnoreCase(lblAux.getText())){
                                            System.out.println("Carta coincide en ");
                                            System.out.println("("+central.getRowIndex(apunNode)+","+central.getColumnIndex(apunNode)+")");
                                            System.out.println(identificadorImgPC[central.getRowIndex(apunNode)][central.getColumnIndex(apunNode)]);
                                            ImageView imgaux = new ImageView(new Image(new File("src/Img/match.png").toURI().toString()));
                                            imgaux.setFitHeight(45);
                                            imgaux.setFitWidth(45);
                                            frmGanar1[central.getRowIndex(apunNode)][central.getColumnIndex(apunNode)] = 1;
                                            auxStackPC[gridPc.getRowIndex(apunNode)][gridPc.getColumnIndex(apunNode)].getChildren().set(1,imgaux );
                                            
                                            if ("COLUMN4".equals(formasG) && existen4ColumnasPc()){
                                                System.out.println("Gano pc por hacer una columna ");
                                                detenJUgador();
                                                System.exit(0);
                                            }else if ("FILAS4".equals(formasG) && existen4FilasPc()){
                                                System.out.println("Gano PC por hacer una fila");
                                                detenJUgador();
                                                System.exit(0);
                                            }else if ("JUNTAS4".equals(formasG) && existen4EsquinasPc()){
                                                System.out.println("Gano PC por hacer una Esquina");
                                                System.exit(0);
                                            }else if("ESQUINAS4".equals(formasG) && existen4PuntasPc()){
                                                System.out.println("Gano PC por hacer 4 Esquina");
                                                System.exit(0);
                                            }else{
                                                    System.out.println("PC cerca");
                                            }
                                                                        }else{
                                          //System.out.println("NO");
                                      }
                                      
                                    }
                                }
                            }
                            i++;
                            
                        }

                      
                        
                    
                });;try {
                    Thread.sleep(3000);
                } catch (InterruptedException ex) {
                    System.out.println(ex.getMessage());
                }
                
                }      
        }
     
     public void detenJUgador(){
         detener = true;
     }
 }

}
