/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import loteriamexicana.Jugador;

/**
 *
 * @author ReiDina
 */
public class PanelPrincipal {
    private BorderPane root;
    private Jugador jugN ;
    public PanelPrincipal() {
        this.root = root;
        root=new BorderPane();
        root.setPadding(new Insets(10));
        fillCenter1();
    }
    public void fillCenter1(){
        
        VBox contIni = new VBox(15);
        
        Label lbl1 = new Label("Bienvenido a Loteria Mexicana");
        lbl1.setAlignment(Pos.CENTER);
        
        Label lbl2 = new Label("Nombre Jugador:");
        lbl2.setPadding(new Insets(0, 0, 0, 15));
        
        TextField txtNombre = new TextField();
        txtNombre.setPromptText("Ingrese Nombre:");
        txtNombre.setPadding(new Insets(0, 0, 0, 15));
        
        Button btn1 = new Button("Comenzar");
        
        btn1.setOnAction((ActionEvent e) -> {
            
            //Alert alt = new Alert(Alert.AlertType.CONFIRMATION);
            
            JuegoMain juego = new JuegoMain();
            Scene scene = new Scene(juego.getRoot(), 1000, 1000);
            
        });
        
        Button btn2 = new Button("Cerrar");

        btn2.setOnAction(e -> {
            System.exit(0);
            
        });
        contIni.getChildren().addAll(lbl1,lbl2,txtNombre,btn1,btn2);
        root.setCenter(contIni);    
    }

    public BorderPane getRoot() {
        return root;
    }

    public void setRoot(BorderPane root) {
        this.root = root;
    }
    
}
